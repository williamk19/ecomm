import {
  FormGroup,
  FormInputLabel,
  FormInputElement,
} from './form-input.styles';
import { FC, InputHTMLAttributes } from 'react';

type FormInputProps = {
  title: string;
} & InputHTMLAttributes<HTMLInputElement>;

const FormInput: FC<FormInputProps> = ({ title, ...otherProps }) => {
  return (
    <FormGroup>
      <FormInputElement {...otherProps} />
      {title && (
        <FormInputLabel
          shrink={Boolean(
            otherProps.value &&
              typeof otherProps.value === 'string' &&
              otherProps.value.length,
          )}>
          {title}
        </FormInputLabel>
      )}
    </FormGroup>
  );
};

export default FormInput;
