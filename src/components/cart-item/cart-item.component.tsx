import {
  CartItemContainer,
  ItemDetails,
  ItemDetailsName,
} from './cart-item.styles';

import { CartItem as TCartItem } from '../../store/cart/cart.type';
import { FC, memo } from 'react';

type CartItemProps = {
  item: TCartItem;
};

const CartItem: FC<CartItemProps> = memo(({ item }) => {
  const { name, imageUrl, quantity, price } = item;

  return (
    <CartItemContainer>
      <img
        src={imageUrl}
        alt={`${name}`}
      />
      <ItemDetails>
        <ItemDetailsName>{name}</ItemDetailsName>
        <span className='price'>
          {quantity} x {price}
        </span>
      </ItemDetails>
    </CartItemContainer>
  );
});

export default CartItem;
