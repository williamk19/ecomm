import { useSelector, useDispatch } from 'react-redux';
import { BUTTON_TYPE_CLASSES } from '../button/button.component';
import { ProductCardButton, ProductCardContainer, ProductCardFooter, ProductCardFooterName, ProductCardFooterPrice, ProductCardImage } from './product-card.styles';
import { selectCartItems } from '../../store/cart/cart.selector';
import { addItemToCart } from '../../store/cart/cart.action';
import { FC } from 'react';
import { CategoryItem } from '../../store/categories/categories.type';

type ProductCardProps = {
  product: CategoryItem;
}

const ProductCard: FC<ProductCardProps> = ({ product }) => {
  const { name, imageUrl, price } = product;
  const cartItems = useSelector(selectCartItems);
  const dispatch = useDispatch();

  const addProductToCart = () => dispatch(addItemToCart(cartItems, product));

  return (
    <ProductCardContainer>
      <ProductCardImage src={imageUrl} alt={`${name}`} />
      <ProductCardButton onClick={addProductToCart} buttonType={BUTTON_TYPE_CLASSES.inverted}>
        Add to cart
      </ProductCardButton>
      <ProductCardFooter>
        <ProductCardFooterName>
          {name}
        </ProductCardFooterName>
        <ProductCardFooterPrice>
          {price}
        </ProductCardFooterPrice>
      </ProductCardFooter>
    </ProductCardContainer>
  );
};

export default ProductCard;