import { useState, ChangeEvent, FormEvent } from 'react';
import { AuthError, AuthErrorCodes } from 'firebase/auth';
import Button from '../button/button.component';
import FormInput from '../form-input/form-input.component';
import { SignUpContainer, SignUpTitle } from './sign-up-form.styles';
import { useDispatch } from 'react-redux';
import { signUpStart } from '../../store/user/user.action';

const defaultFormFields = {
  displayName: '',
  email: '',
  password: '',
  confirmPassword: '',
};

const SignUpForm = () => {
  const [formFields, setFormFields] = useState(defaultFormFields);
  const { displayName, email, password, confirmPassword } = formFields;
  const dispatch = useDispatch();

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormFields({ ...formFields, [name]: value });
  };

  const resetFormFields = () => {
    setFormFields(defaultFormFields);
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (password !== confirmPassword) return alert('Passwords does not match');

    try {
      dispatch(signUpStart(email, password, displayName));
      resetFormFields();
    } catch (error) {
      if ((error as AuthError).code === AuthErrorCodes.EMAIL_EXISTS) {
        alert('Cannot create user');
      } else {
        console.log(`An error occured ${error as Error}`);
      }
    }
  };

  return (
    <SignUpContainer>
      <SignUpTitle>Don't have an account</SignUpTitle>
      <span>Sign up with your email and password</span>
      <form
        onSubmit={handleSubmit}
        method='POST'>
        <FormInput
          name='displayName'
          title='Display Name'
          value={displayName}
          onChange={handleChange}
        />

        <FormInput
          name='email'
          title='Email'
          value={email}
          onChange={handleChange}
          type='email'
        />

        <FormInput
          name='password'
          title='Password'
          value={password}
          onChange={handleChange}
          type='password'
        />

        <FormInput
          name='confirmPassword'
          title='Confirm Password'
          value={confirmPassword}
          onChange={handleChange}
          type='password'
        />

        <Button type='submit'>Sign Up</Button>
      </form>
    </SignUpContainer>
  );
};

export default SignUpForm;
