import { render, screen } from '@testing-library/react';
import Button from '../button.component';

describe('button tests', () => {
  test('render base button when nothing is passed', () => {
    render(<Button>Test</Button>);
    const buttonElement = screen.getByRole('button');
    expect(buttonElement).toHaveStyle('background-color: black');
  });

  test('render google sign in button when google-sign-in passed', () => {
    render(<Button buttonType='google-sign-in'>Google Sign In</Button>);
    const buttonElement = screen.getByRole('button');
    expect(buttonElement).toHaveStyle('background-color: #4285f4');
  });

  test('render inverted button when inverted passed', () => {
    render(<Button buttonType='inverted'>Inverted</Button>);
    const buttonElement = screen.getByRole('button');
    expect(buttonElement).toHaveStyle('background-color: white');
  });

  test('should be disabled if loading is true', () => {
    render(<Button isLoading={true}>Test</Button>);
    const buttonElement = screen.getByRole('button');
    expect(buttonElement).toBeDisabled();
  })
});