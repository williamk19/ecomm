import { Key } from 'react';
import CategoryItem from '../category-item/category-item.component';
import { CategoriesContainerWrapper } from './categories-container.styles';

type Category = {
  id: Key;
  title: string;
  imageUrl: string;
  route: string;
};

const CATEGORIES: Category[] = [
  {
    id: 1,
    title: 'hats',
    imageUrl: 'https://i.ibb.co/cvpntL1/hats.png',
    route: '/hats',
  },
  {
    id: 2,
    title: 'jackets',
    imageUrl: 'https://i.ibb.co/px2tCc3/jackets.png',
    route: '/jackets',
  },
  {
    id: 3,
    title: 'sneakers',
    imageUrl: 'https://i.ibb.co/0jqHpnp/sneakers.png',
    route: '/sneakers',
  },
  {
    id: 4,
    title: 'womens',
    imageUrl: 'https://i.ibb.co/GCCdy8t/womens.png',
    route: '/womens',
  },
  {
    id: 5,
    title: 'mens',
    imageUrl: 'https://i.ibb.co/R70vBrQ/men.png',
    route: '/mens',
  },
];

const CategoriesContainer = () => {
  return (
    <CategoriesContainerWrapper>
      {CATEGORIES.map((category) => (
        <CategoryItem
          key={category.id}
          category={category}
        />
      ))}
    </CategoriesContainerWrapper>
  );
};

export default CategoriesContainer;
