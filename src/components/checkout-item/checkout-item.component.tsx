import {
  CheckoutImage,
  CheckoutItemContainer,
  CheckoutName,
  CheckoutPrice,
  CheckoutQuantity,
  CheckoutQuantityArrow,
  CheckoutQuantityValue,
  ImageContainer,
  RemoveButton,
} from './checkout-item.styles';
import { useDispatch, useSelector } from 'react-redux';
import { selectCartItems } from '../../store/cart/cart.selector';
import {
  addItemToCart,
  clearItemFromCart,
  removeItemFromCart,
} from '../../store/cart/cart.action';
import { CartItem } from '../../store/cart/cart.type';
import { FC, memo } from 'react';

type CheckoutItemProps = {
  item: CartItem;
}

const CheckoutItem: FC<CheckoutItemProps> = memo(({ item }) => {
  const { name, quantity, price, imageUrl } = item;
  const dispatch = useDispatch();
  const cartItems = useSelector(selectCartItems);

  const removeItemHandler = () => {
    dispatch(removeItemFromCart(cartItems, item));
  };

  const clearItemHandler = () => {
    dispatch(clearItemFromCart(cartItems, item));
  };

  const addItemHandler = () => {
    dispatch(addItemToCart(cartItems, item));
  };

  return (
    <CheckoutItemContainer>
      <ImageContainer>
        <CheckoutImage
          src={imageUrl}
          alt={`${name}`}
        />
      </ImageContainer>
      <CheckoutName>{name}</CheckoutName>
      <CheckoutQuantity>
        <CheckoutQuantityArrow onClick={removeItemHandler}>
          &#x3008;
        </CheckoutQuantityArrow>
        <CheckoutQuantityValue>{quantity}</CheckoutQuantityValue>
        <CheckoutQuantityArrow onClick={addItemHandler}>
          &#x3009;
        </CheckoutQuantityArrow>
      </CheckoutQuantity>
      <CheckoutPrice>{price}</CheckoutPrice>
      <RemoveButton onClick={clearItemHandler}>&#10005;</RemoveButton>
    </CheckoutItemContainer>
  );
});

export default CheckoutItem;
