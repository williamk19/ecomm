import styled from 'styled-components';

export const CheckoutItemContainer = styled.div`
  width: 100%;
  display: flex;
  min-height: 100px;
  border-bottom: 1px solid darkgrey;
  padding: 15px 0;
  font-size: 20px;
  align-items: center;
`;

export const ImageContainer = styled.div`
  width: 23%;
  padding-right: 15px;
`;

export const CheckoutImage = styled.img`
  height: 100%;
  width: 100%;
`;

export const CheckoutName = styled.div`
  width: 23%;
`;

export const CheckoutQuantity = styled.div`
  width: 23%;
  display: flex;
`;

export const CheckoutQuantityArrow = styled.span`
  cursor: pointer;
  user-select: none;
`;

export const CheckoutQuantityValue = styled.span`
  margin: 0 10px;
  user-select: none;
`;

export const CheckoutPrice = styled.div`
  width: 23%;
`;

export const RemoveButton = styled.div`
  padding-left: 12px;
  cursor: pointer;
  user-select: none;
`;

// .checkout-item-container {
//   width: 100%;
//   display: flex;
//   min-height: 100px;
//   border-bottom: 1px solid darkgrey;
//   padding: 15px 0;
//   font-size: 20px;
//   align-items: center;

//   .image-container {
//     width: 23%;
//     padding-right: 15px;

//     img {
//       width: 100%;
//       height: 100%;
//     }
//   }
//   .name,
//   .quantity,
//   .price {
//     width: 23%;
//   }

//   .quantity {
//     display: flex;

//     .arrow {
//       cursor: pointer;
//       user-select: none;
//     }

//     .value {
//       margin: 0 10px;
//       user-select: none;
//     }
//   }

//   .remove-button {
//     padding-left: 12px;
//     cursor: pointer;
//     user-select: none;
//   }
// }
