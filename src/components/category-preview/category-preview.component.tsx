import ProductCard from '../product-card/product-card.component';
import { Link } from 'react-router-dom';
import {
  CategoryPreviewContainer,
  CategoryPreviewElement,
  CategoryPreviewTitle,
} from './category-preview.styles';
import { FC } from 'react';
import { CategoryItem } from '../../store/categories/categories.type';

type CategoryPreviewProps = {
  title: string;
  products: CategoryItem[];
};

const CategoryPreview: FC<CategoryPreviewProps> = ({ title, products }) => {
  return (
    <CategoryPreviewContainer>
      <h2>
        <CategoryPreviewTitle>
          <Link to={`/shop/${title}`}>{title.toUpperCase()}</Link>
        </CategoryPreviewTitle>
      </h2>
      <CategoryPreviewElement>
        {products
          .filter((_, idx) => idx < 4)
          .map((product) => (
            <ProductCard
              key={product.id}
              product={product}
            />
          ))}
      </CategoryPreviewElement>
    </CategoryPreviewContainer>
  );
};

export default CategoryPreview;
