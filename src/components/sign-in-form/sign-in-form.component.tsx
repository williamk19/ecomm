import { useState, FormEvent, ChangeEvent } from 'react';
import Button, { BUTTON_TYPE_CLASSES } from '../button/button.component';
import FormInput from '../form-input/form-input.component';
import {
  ButtonSigninWrapper,
  SignInContainer,
  SignInTitle,
} from './sign-in-form.styles';
import { useDispatch } from 'react-redux';
import {
  googleSignInStart,
  emailSignInStart,
} from '../../store/user/user.action';

const defaultFormFields = {
  email: '',
  password: '',
};

const SignInForm = () => {
  const [formFields, setFormFields] = useState(defaultFormFields);
  const { email, password } = formFields;
  const dispatch = useDispatch();

  const logGoogleUser = async () => {
    dispatch(googleSignInStart());
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormFields({ ...formFields, [name]: value });
  };

  const resetFormFields = () => {
    setFormFields(defaultFormFields);
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      dispatch(emailSignInStart(email, password));
      resetFormFields();
    } catch (error) {
      console.error('User sign in failed');
    }
  };

  return (
    <SignInContainer>
      <SignInTitle>Already have an account</SignInTitle>
      <span>Sign in with your email and password</span>
      <form
        onSubmit={(e) => handleSubmit(e)}
        method='POST'>
        <FormInput
          name='email'
          title='Email'
          value={email}
          onChange={(e) => handleChange(e)}
          type='email'
        />

        <FormInput
          name='password'
          title='Password'
          value={password}
          onChange={(e) => handleChange(e)}
          type='password'
        />

        <ButtonSigninWrapper className='button-signin-wrapper'>
          <Button type='submit'>Sign In</Button>
          <Button
            type='button'
            buttonType={BUTTON_TYPE_CLASSES.google}
            onClick={logGoogleUser}>
            Google Sign In
          </Button>
        </ButtonSigninWrapper>
      </form>
    </SignInContainer>
  );
};

export default SignInForm;
