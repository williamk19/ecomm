import styled from 'styled-components';

export const SignInContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 380px;
`;

export const ButtonSigninWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const SignInTitle = styled.h2`
  margin: 10px 0;
`;