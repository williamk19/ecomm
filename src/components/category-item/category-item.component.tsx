import { FC } from 'react';
import {
  BackgroundImage,
  CategoryBodyContainer,
  CategoryItemContainer,
} from './category-item.styles';
import { useNavigate } from 'react-router-dom';

type CategoryItemProps = {
  category: {
    title: string;
    imageUrl: string;
  };
};

const CategoryItem: FC<CategoryItemProps> = ({ category }) => {
  const { title, imageUrl } = category;
  const navigate = useNavigate();

  const homeNavigateHandler = () => {
    navigate(`/shop/${title.toLowerCase()}`);
  };

  return (
    <CategoryItemContainer>
      <BackgroundImage imageUrl={imageUrl} />
      <CategoryBodyContainer onClick={homeNavigateHandler}>
        <h2>{title.toUpperCase()}</h2>
        <p>Shop Now</p>
      </CategoryBodyContainer>
    </CategoryItemContainer>
  );
};

export default CategoryItem;
