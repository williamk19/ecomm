import { screen } from '@testing-library/react';
import { renderWithProviders } from '../../../utils/test/test.utils';
import CartIcon from '../cart-icon.component';

describe('cart icon tests', () => {
  test('use preloaded state to render', () => {
    const initialCartItems = [
      { id: 1, name: 'Item A', imageUrl: 'test', price: 10, quantity: 2 },
      { id: 2, name: 'Item B', imageUrl: 'test', price: 10, quantity: 2 },
    ];

    renderWithProviders(<CartIcon />, {
      preloadedState: {
        cart: {
          cartItems: initialCartItems
        }
      }
    });

    const cartIconElement = screen.getByText('4');
    expect(cartIconElement).toBeInTheDocument();
  });
});