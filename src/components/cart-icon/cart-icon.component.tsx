import {
  CartIconContainer,
  CartIconCount,
  ShoppingIconComponent,
} from './cart-icon.styles';
import { useDispatch, useSelector } from 'react-redux';
import { setIsCartOpen } from '../../store/cart/cart.action';
import {
  selectCartCount,
  selectIsCartOpen,
} from '../../store/cart/cart.selector';

const CartIcon = () => {
  const totalQuantity = useSelector(selectCartCount);
  const dispatch = useDispatch();
  const isCartOpen = useSelector(selectIsCartOpen);

  const setIsCartOpenHandler = () => dispatch(setIsCartOpen(isCartOpen));

  return (
    <CartIconContainer onClick={setIsCartOpenHandler}>
      <ShoppingIconComponent />
      <CartIconCount>{totalQuantity}</CartIconCount>
    </CartIconContainer>
  );
};

export default CartIcon;
