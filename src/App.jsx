import { Routes, Route } from 'react-router-dom';
import Spinner from './components/spinner/spinner.component';
import { useEffect, lazy, Suspense } from 'react';
import { checkUserSession } from './store/user/user.action';
import { useDispatch } from 'react-redux';
import { GlobalStyle } from './global.styles';

const Shop = lazy(() => import('./routes/shop/shop.component'));
const Navigation = lazy(() =>
  import('./routes/navigation/navigation.component'),
);
const Checkout = lazy(() => import('./routes/checkout/checkout.component'));
const Home = lazy(() => import('./routes/home/home.component'));
const Auth = lazy(() => import('./routes/auth/auth.component'));

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkUserSession());
  }, [dispatch]);

  return (
    <Suspense fallback={<Spinner />}>
      <GlobalStyle />
      <Routes>
        <Route
          path='/'
          element={<Navigation />}>
          <Route
            index
            element={<Home />}
          />
          <Route
            path='shop/*'
            element={<Shop />}
          />
          <Route
            path='auth'
            element={<Auth />}
          />
          <Route
            path='checkout'
            element={<Checkout />}
          />
        </Route>
      </Routes>
    </Suspense>
  );
};

export default App;
