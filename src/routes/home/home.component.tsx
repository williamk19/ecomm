import CategoriesContainer from '../../components/categories-container/categories-container.component';
import { Outlet } from 'react-router-dom';

const Home = () => {
  return (
    <>
      <CategoriesContainer />
      <Outlet />
    </>
  );
};

export default Home;
