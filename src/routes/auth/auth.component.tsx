import SignUpForm from '../../components/sign-up-form/sign-up-form.component';
import SignInForm from '../../components/sign-in-form/sign-in-form.component';
import { SignInPage } from './auth.styles';

const Auth = () => {
  return (
    <SignInPage>
      <SignInForm />
      <SignUpForm />
    </SignInPage>
  );
};

export default Auth;
