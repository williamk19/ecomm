import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import ProductCard from '../../components/product-card/product-card.component';
import {
  CategoryContainer,
  CategoryTitle,
  ShopContainer,
} from './category.styles';
import { useSelector } from 'react-redux';
import {
  selectCategoriesIsLoading,
  selectCategoriesMap,
} from '../../store/categories/categories.selector';
import Spinner from '../../components/spinner/spinner.component';

type CategoryRouteParams = {
  category: string;
};

const Category = () => {
  const { category } = useParams<
    keyof CategoryRouteParams
  >() as CategoryRouteParams;
  const categoriesMap = useSelector(selectCategoriesMap);
  const categoriesIsLoading = useSelector(selectCategoriesIsLoading);
  const [products, setProducts] = useState(categoriesMap[category]);

  useEffect(() => {
    setProducts(categoriesMap[category]);
  }, [category, categoriesMap]);

  return (
    <ShopContainer>
      <CategoryTitle>
        <span>{category.toUpperCase()}</span>
      </CategoryTitle>
      {categoriesIsLoading === true ? (
        <Spinner />
      ) : (
        <CategoryContainer>
          {products?.map((product) => (
            <ProductCard
              key={product.id}
              product={product}
            />
          ))}
        </CategoryContainer>
      )}
    </ShopContainer>
  );
};

export default Category;
