import { CategoryItem } from '../categories/categories.type';
import { CartItem } from './cart.type';

export const addCartItem = (
  cartItems: CartItem[],
  productToAdd: CategoryItem,
): CartItem[] => {
  const productInCart = cartItems.find((item) => item.id === productToAdd.id);

  if (productInCart) {
    return cartItems.map((cartItem) =>
      cartItem.id === productToAdd.id
        ? { ...cartItem, quantity: cartItem.quantity + 1 }
        : cartItem,
    );
  } else {
    return [...cartItems, { ...productToAdd, quantity: 1 }];
  }
};

export const clearCartItem = (
  cartItems: CartItem[],
  product: CartItem,
): CartItem[] => {
  const productInCart = cartItems.find((item) => item.id === product.id);

  if (productInCart) {
    return cartItems.filter((cartItem) => cartItem.id !== product.id);
  }

  return cartItems;
};

export const removeCartItem = (
  cartItems: CartItem[],
  product: CartItem,
): CartItem[] => {
  const productInCart = cartItems.find((item) => item.id === product.id);

  if (productInCart && productInCart.quantity > 1) {
    return cartItems.map((cartItem) =>
      cartItem.id === product.id
        ? { ...cartItem, quantity: cartItem.quantity - 1 }
        : cartItem,
    );
  } else {
    return cartItems.filter((cartItem) => cartItem.id !== product.id);
  }
};
