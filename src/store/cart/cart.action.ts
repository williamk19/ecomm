import {
  ActionWithPayload,
  createAction,
  withMatcher,
} from '../../utils/reducer/reducers.utils';
import { CART_ACTION_TYPE, CartItem } from './cart.type';
import { addCartItem, clearCartItem, removeCartItem } from './cart.helper';
import { CategoryItem } from '../categories/categories.type';

export type SetIsCartOpen = ActionWithPayload<
  CART_ACTION_TYPE.SET_IS_CART_OPEN,
  boolean
>;

export type SetCartItem = ActionWithPayload<
  CART_ACTION_TYPE.SET_CART_ITEM,
  CartItem[]
>;

export const setIsCartOpen = withMatcher(
  (isCartOpen: boolean): SetIsCartOpen => {
    const payload: boolean = !isCartOpen;
    return createAction(CART_ACTION_TYPE.SET_IS_CART_OPEN, payload);
  },
);

export const setCartItem = withMatcher((cartItems: CartItem[]): SetCartItem => {
  return createAction(CART_ACTION_TYPE.SET_CART_ITEM, cartItems);
});

export const addItemToCart = (cartItems: CartItem[], product: CategoryItem) => {
  const newCartItems = addCartItem(cartItems, product);
  return createAction(CART_ACTION_TYPE.SET_CART_ITEM, newCartItems);
};

export const removeItemFromCart = (
  cartItems: CartItem[],
  product: CartItem,
) => {
  const newCartItems: CartItem[] = removeCartItem(cartItems, product);
  return createAction(CART_ACTION_TYPE.SET_CART_ITEM, newCartItems);
};

export const clearItemFromCart = (cartItems: CartItem[], product: CartItem) => {
  const newCartItems: CartItem[] = clearCartItem(cartItems, product);
  return createAction(CART_ACTION_TYPE.SET_CART_ITEM, newCartItems);
};
