import {
  categoriesReducer,
  CATEGORIES_INITIAL_STATE
} from '../categories.reducer';
import {
  fetchCategoriesStart,
  fetchCategoriesSuccess,
  fetchCategoriesFailed
} from '../categories.action';

describe('category reducer test', () => {
  test('fetch categories start', () => {
    const expectedState = {
      ...CATEGORIES_INITIAL_STATE,
      isLoading: true
    };

    expect(categoriesReducer(CATEGORIES_INITIAL_STATE, fetchCategoriesStart()))
      .toEqual(expectedState);
  });

  test('fetch categories success', () => {
    const mockData = [
      {
        title: 'mens',
        imageUrl: 'test',
        items: [
          { id: 1, name: "Product 1" }
        ]
      },
      {
        title: 'womens',
        imageUrl: 'test',
        items: [
          { id: 2, name: "Product 2" }
        ]
      }
    ];

    const expectedState = {
      ...CATEGORIES_INITIAL_STATE,
      isLoading: false,
      categories: mockData
    };

    expect(categoriesReducer(CATEGORIES_INITIAL_STATE, fetchCategoriesSuccess(mockData)))
      .toEqual(expectedState);
  });

  test('fetch categories failed', () => {
    const mockError = new Error('error fetching categories');

    const expectedState = {
      ...CATEGORIES_INITIAL_STATE,
      isLoading: false,
      error: mockError,
    };

    expect(categoriesReducer(CATEGORIES_INITIAL_STATE, fetchCategoriesFailed(mockError)))
      .toEqual(expectedState);
  });
});