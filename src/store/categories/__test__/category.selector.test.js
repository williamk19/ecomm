import {
  selectCategories,
  selectCategoriesIsLoading,
  selectCategoriesMap
} from '../categories.selector';

const mockState = {
  categories: {
    isLoading: false,
    categories: [
      {
        title: 'mens',
        imageUrl: 'test',
        items: [
          { id: 1, name: "Product 1" }
        ]
      },
      {
        title: 'womens',
        imageUrl: 'test',
        items: [
          { id: 2, name: "Product 2" }
        ]
      }
    ]
  }
};

describe('categories selector tests', () => {
  test('selectCategories should return the categoriesData', () => {
    const categoriesSlice = selectCategories(mockState);
    expect(categoriesSlice).toEqual(mockState.categories.categories);
  });

  test('selectCategoriesIsLoading should return isLoading state', () => {
    const isLoading = selectCategoriesIsLoading(mockState);
    expect(isLoading).toEqual(false);
  });

  test('selectCategoriesMap should convert items array into map', () => {
    const expectedCategoriesMap = {
      mens: [
        { id: 1, name: "Product 1" }
      ],
      womens: [
        { id: 2, name: "Product 2" }
      ]
    };

    const categoriesMap = selectCategoriesMap(mockState);
    expect(categoriesMap).toEqual(expectedCategoriesMap);
  });
});