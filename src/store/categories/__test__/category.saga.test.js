import { expectSaga, testSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';
import {
  fetchCategoriesAsync,
  onFetchCategories,
  categoriesSaga
} from '../categories.saga';
import { call } from 'redux-saga/effects';
import { CATEGORIES_ACTION_TYPE } from '../categories.type';
import { getCategoriesAndDocument } from '../../../utils/firebase/firebase.utils';
import { fetchCategoriesFailed, fetchCategoriesSuccess } from '../categories.action';

describe('categories saga tests', () => {
  test('categoriesSaga', () => {
    testSaga(categoriesSaga)
      .next()
      .all([call(onFetchCategories)])
      .next()
      .isDone();
  });

  test('onFetchCategories', () => {
    testSaga(onFetchCategories)
      .next()
      .takeLatest(CATEGORIES_ACTION_TYPE.FETCH_CATEGORIES_START, fetchCategoriesAsync)
      .next()
      .isDone();
  });

  test('fetchCategoriesAsync success', () => {
    const mockCategoriesArray = [
      { id: 1, name: 'category 1' },
      { id: 2, name: 'category 2' }
    ];

    return expectSaga(fetchCategoriesAsync)
      .provide([[call(getCategoriesAndDocument), mockCategoriesArray]])
      .put(fetchCategoriesSuccess(mockCategoriesArray))
      .run();
  });

  test('fetchCategoriesAsync fail', () => {
    const mockError = new Error('an error');
    return expectSaga(fetchCategoriesAsync)
      .provide([[call(getCategoriesAndDocument), throwError(mockError)]])
      .put(fetchCategoriesFailed(mockError))
      .run();
  });
});